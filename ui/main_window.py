from PySide2 import QtWidgets, QtGui


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, title, icon, parent=None):
        super().__init__(parent)
        self.setWindowTitle(title)
        self.setWindowIcon(icon)
        self.resize(800, 600)

        #meni
        self.menu_bar = QtWidgets.QMenuBar(self)
        #toolbar
        self.tool_bar = QtWidgets.QToolBar("Toolbar",self)
        #statusbar
        self.status_bar = QtWidgets.QStatusBar(self)
        #centralwidget
        self.central_widget = QtWidgets.QStackedWidget(self)
        #widgets in stacked widget
        self.widgets = {}

        self.document = None # otvoreni dokument (trenutno aktivni dokument)

        self.actions_dict = {
            # FIXME: ispraviti ikonicu na X
            "quit": QtWidgets.QAction(QtGui.QIcon("resources/icons/document.png"), "&Quit", self)
            # TODO: dodati i ostale akcije za help i za npr. osnovno za dokument
            # dodati open...
        }

        self._bind_actions()

        self._populate_menu_bar()

        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)

    def _populate_menu_bar(self):
        file_menu = QtWidgets.QMenu("&File")
        help_menu = QtWidgets.QMenu("&Help")

        file_menu.addAction(self.actions_dict["quit"])

        self.menu_bar.addMenu(file_menu)
        self.menu_bar.addMenu(help_menu)

    def _bind_actions(self):
        self.actions_dict["quit"].setShortcut("Ctrl+Q")
        self.actions_dict["quit"].triggered.connect(self.close)

    def add_widget(self, widget):
        """
        Adds widget to central (stack) widget
        """
        self.widgets[widget.widget_for] = self.central_widget.currentIndex()
        self.central_widget.addWidget(widget)

    def remove_widget(self, widget):
        self.central_widget.removeWidget(widget)

    # TODO: dodati metodu koja ce u fokus staviti trenutko aktivni plugin (njegov widget)